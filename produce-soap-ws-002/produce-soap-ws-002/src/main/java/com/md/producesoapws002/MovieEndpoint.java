package com.md.producesoapws002;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.md.generated.GetAllMoviesResponse;
import com.md.generated.GetMoviesByDirecorNameResponse;
import com.md.generated.GetMoviesByDirectorNameRequest;
import com.md.generated.GetMoviesByGenreRequest;
import com.md.generated.GetMoviesByGenreResponse;
import com.md.generated.GetMoviesByYearRequest;
import com.md.generated.GetMoviesByYearResponse;
import com.md.generated.Movie;

@Endpoint
public class MovieEndpoint {
	
	private static final String NAMESPACE_URI = "http://www.example.org/movies/";
	
	private MovieRepository movieRepository;

	@Autowired
	public MovieEndpoint(MovieRepository movieRepository) {
		this.movieRepository = movieRepository;
	}
	
	@PayloadRoot(namespace=NAMESPACE_URI, localPart="getAllMoviesRequest")
	@ResponsePayload
	public GetAllMoviesResponse getAllMovies() {
		GetAllMoviesResponse response = new GetAllMoviesResponse();
		List<Movie> movieList = movieRepository.getAllMovies();
		response.getMovies().addAll(movieList);
		return response;
	}
	
	@PayloadRoot(namespace=NAMESPACE_URI, localPart="getMoviesByDirectorNameRequest")
	@ResponsePayload
	public GetMoviesByDirecorNameResponse getMoviesByDirectorNameRequest(@RequestPayload GetMoviesByDirectorNameRequest request) {
		GetMoviesByDirecorNameResponse response = new GetMoviesByDirecorNameResponse();
		List<Movie> movieList = movieRepository.getMoviesByDirectorName(request.getDirectorName());
		response.getMovies().addAll(movieList);
		return response;
	}
	
	@PayloadRoot(namespace=NAMESPACE_URI, localPart="getMoviesByGenreRequest")
	@ResponsePayload
	public GetMoviesByGenreResponse getMoviesByGenreRequest(@RequestPayload GetMoviesByGenreRequest request) {
		GetMoviesByGenreResponse response = new GetMoviesByGenreResponse();
		List<Movie> movieList = movieRepository.getMoviesByGenre(request.getGenre());
		response.getMovies().addAll(movieList);
		return response;
	}
	
	@PayloadRoot(namespace=NAMESPACE_URI, localPart="getMoviesByYearRequest")
	@ResponsePayload
	public GetMoviesByYearResponse getMoviesByYearRequest(@RequestPayload GetMoviesByYearRequest request) {
		GetMoviesByYearResponse response = new GetMoviesByYearResponse();
		List<Movie> movieList = movieRepository.getMoviesByYear(request.getYear());
		response.getMovies().addAll(movieList);
		return response;
	}

}
