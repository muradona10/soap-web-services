package com.md.producesoapws002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProduceSoapWs002Application {

	public static void main(String[] args) {
		SpringApplication.run(ProduceSoapWs002Application.class, args);
	}

}
