package com.md.consumesoapws;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.md.schemas.country.GetCountryRequest;
import com.md.schemas.country.GetCountryResponse;

@SpringBootApplication
public class ConsumeSoapWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumeSoapWsApplication.class, args);
	}

	@Bean
	CommandLineRunner lookUp(SOAPConnector soapConnector) {
		return args -> {
			String countryName = "USA";
			if(args.length>0) {
				countryName = args[0];
			}
			
			GetCountryRequest request = new GetCountryRequest();
			request.setName(countryName);
			GetCountryResponse response = (GetCountryResponse) soapConnector.callWS("http://localhost:8091/service/country-details", request);
			
			System.out.println("-----------------------------------------------------------------------");
			System.out.println("Country   :" + response.getCountry().getName());
			System.out.println("Population:" + response.getCountry().getPopulation());
			System.out.println("Capital   :" + response.getCountry().getCapital());
			System.out.println("Currency  :" + response.getCountry().getCurrency());
			System.out.println("-----------------------------------------------------------------------");
		};
	}
	
}
