//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.25 at 07:35:26 PM EET 
//


package com.md.schemas.movie;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.md.schemas.movie package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAllMoviesRequest_QNAME = new QName("http://www.example.org/movies/", "getAllMoviesRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.md.schemas.movie
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllMoviesResponse }
     * 
     */
    public GetAllMoviesResponse createGetAllMoviesResponse() {
        return new GetAllMoviesResponse();
    }

    /**
     * Create an instance of {@link Movie }
     * 
     */
    public Movie createMovie() {
        return new Movie();
    }

    /**
     * Create an instance of {@link GetMoviesByDirectorNameRequest }
     * 
     */
    public GetMoviesByDirectorNameRequest createGetMoviesByDirectorNameRequest() {
        return new GetMoviesByDirectorNameRequest();
    }

    /**
     * Create an instance of {@link GetMoviesByDirecorNameResponse }
     * 
     */
    public GetMoviesByDirecorNameResponse createGetMoviesByDirecorNameResponse() {
        return new GetMoviesByDirecorNameResponse();
    }

    /**
     * Create an instance of {@link GetMoviesByGenreRequest }
     * 
     */
    public GetMoviesByGenreRequest createGetMoviesByGenreRequest() {
        return new GetMoviesByGenreRequest();
    }

    /**
     * Create an instance of {@link GetMoviesByGenreResponse }
     * 
     */
    public GetMoviesByGenreResponse createGetMoviesByGenreResponse() {
        return new GetMoviesByGenreResponse();
    }

    /**
     * Create an instance of {@link GetMoviesByYearRequest }
     * 
     */
    public GetMoviesByYearRequest createGetMoviesByYearRequest() {
        return new GetMoviesByYearRequest();
    }

    /**
     * Create an instance of {@link GetMoviesByYearResponse }
     * 
     */
    public GetMoviesByYearResponse createGetMoviesByYearResponse() {
        return new GetMoviesByYearResponse();
    }

    /**
     * Create an instance of {@link Genre }
     * 
     */
    public Genre createGenre() {
        return new Genre();
    }

    /**
     * Create an instance of {@link Director }
     * 
     */
    public Director createDirector() {
        return new Director();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.example.org/movies/", name = "getAllMoviesRequest")
    public JAXBElement<Object> createGetAllMoviesRequest(Object value) {
        return new JAXBElement<Object>(_GetAllMoviesRequest_QNAME, Object.class, null, value);
    }

}
