package com.md.consumesoapws002;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.md.schemas.movie.GetAllMoviesResponse;
import com.md.schemas.movie.GetMoviesByDirecorNameResponse;
import com.md.schemas.movie.GetMoviesByDirectorNameRequest;
import com.md.schemas.movie.GetMoviesByGenreRequest;
import com.md.schemas.movie.GetMoviesByGenreResponse;
import com.md.schemas.movie.GetMoviesByYearRequest;
import com.md.schemas.movie.GetMoviesByYearResponse;
import com.md.schemas.movie.Movie;

@SpringBootApplication
public class ConsumeSoapWs002Application {

	public static void main(String[] args) {
		SpringApplication.run(ConsumeSoapWs002Application.class, args);
	}
	
	private static final String WS_ENDPOINT_URI = "http://localhost:8093/service/movies";
	
	@Bean
	CommandLineRunner lookUp(SOAPConnector soapConnector) {
		
		return args -> {
			
			//getAllMovies(soapConnector);
			getMoviesByGenre(soapConnector, "Comedy");
			getMoviesByDirectorName(soapConnector, "Reha Erdem");
			getMoviesByYear(soapConnector, 2007);
			
		};
		
	}
	
	private void getAllMovies(SOAPConnector connector) {
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("------------------------   ALL MOVIES   -------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		GetAllMoviesResponse response = new GetAllMoviesResponse();
		response = (GetAllMoviesResponse) connector.callWS(WS_ENDPOINT_URI, null);
		
		List<Movie> movies = response.getMovies();
		printMovies(movies);
	}

	private void getMoviesByDirectorName(SOAPConnector connector, String directorName) {
		List<Movie> movies;
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("---------------------   MOVIES BY DIRECTOR  ---------------------------");
		System.out.println("-----------------------------------------------------------------------");
		GetMoviesByDirectorNameRequest request = new GetMoviesByDirectorNameRequest();
		request.setDirectorName(directorName);
		GetMoviesByDirecorNameResponse response = new GetMoviesByDirecorNameResponse();
		response = (GetMoviesByDirecorNameResponse) connector.callWS(WS_ENDPOINT_URI, request);
		
		movies = response.getMovies();
		printMovies(movies);
	}

	private void getMoviesByGenre(SOAPConnector connector, String genre) {
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("---------------------   MOVIES BY GENRE   -----------------------------");
		System.out.println("-----------------------------------------------------------------------");
		GetMoviesByGenreRequest request = new GetMoviesByGenreRequest();
		request.setGenre(genre);
		GetMoviesByGenreResponse response = new GetMoviesByGenreResponse();
		response = (GetMoviesByGenreResponse) connector.callWS(WS_ENDPOINT_URI, request);
		
		List<Movie> movies = response.getMovies();
		printMovies(movies);
	}
	
	private void getMoviesByYear(SOAPConnector connector, int year) {
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("---------------------   MOVIES BY YEAR   ------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		GetMoviesByYearRequest request = new GetMoviesByYearRequest();
		request.setYear(year);
		GetMoviesByYearResponse response = new GetMoviesByYearResponse();
		response = (GetMoviesByYearResponse) connector.callWS(WS_ENDPOINT_URI, request);
		
		List<Movie> movies = response.getMovies();
		printMovies(movies);
	}

	private void printMovies(List<Movie> movies) {
		movies.stream().forEach(movie -> {
			System.out.println("-----------------------------------------------------------------------");
			System.out.println("Id   : " + movie.getId());
			System.out.println("Title: " + movie.getTitle());
			System.out.println("Year : " + movie.getYear());
			System.out.println("IMDB : " + movie.getImdb());
			
			System.out.println("GENRES:");
			movie.getGenres().forEach(genre -> {
				System.out.println("Genre: " + genre.getName());
			});
			
			System.out.println("DIRECTORS:");
			movie.getDirectors().forEach(director -> {
				System.out.println("Name : " + director.getName());
				System.out.println("IMDB : " + director.getImdb());
			});
		});
	}

}
