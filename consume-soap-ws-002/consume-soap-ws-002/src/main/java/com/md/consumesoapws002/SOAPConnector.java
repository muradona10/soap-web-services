package com.md.consumesoapws002;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class SOAPConnector extends WebServiceGatewaySupport {
	
	public Object callWS(String url, Object request) {
		return getWebServiceTemplate().marshalSendAndReceive(url, request);
	}

}
