package com.md.producesoapws;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.md.generated.Country;
import com.md.generated.Currency;

@Component
public class CountryRepository {
	
	private static final Map<String, Country> countries = new HashMap<>();
	
	@PostConstruct
	public void initData() {
		
		Country turkey = new Country();
		turkey.setName("Türkiye");
		turkey.setPopulation(81000000);
		turkey.setCapital("Ankara");
		turkey.setCurrency(Currency.TL);
		countries.put(turkey.getName(), turkey);
		
		Country usa = new Country();
		usa.setName("USA");
		usa.setCapital("Washington");
		usa.setCurrency(Currency.USD);
		usa.setPopulation(328000000);
		countries.put(usa.getName(), usa);

		Country uk = new Country();
		uk.setName("United Kingdom");
		uk.setCapital("London");
		uk.setCurrency(Currency.GBP);
		uk.setPopulation(63705000);

		countries.put(uk.getName(), uk);
		
	}
	
	public Country findCountry(String countryName) {
		Assert.notNull(countryName,"Country Name cannot be null");
		return countries.get(countryName);
	}
	

}
