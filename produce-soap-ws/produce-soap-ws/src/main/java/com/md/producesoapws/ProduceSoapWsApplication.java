package com.md.producesoapws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProduceSoapWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProduceSoapWsApplication.class, args);
	}

}
